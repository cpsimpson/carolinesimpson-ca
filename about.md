---
layout: page
title: About
permalink: /about/
nav-menu: true
---

Welcome to my personal website! My name is Caroline and I am a quirky blend of nerdy, mind-curious, and crafty cat-person.

By day I'm and Engineering Manager at GitLab. Note: All of the opinions expressed in this website are my own. I have been a software developer using many technologies over the years. My personal favourite language is Python. 

One of my passions is delving into the mysteries of the mind. As a psychology student, I am constantly on the lookout for ways to understand the human experience better. I believe that understanding how people think and feel is key to life and relationships.

When I am not busy sitting at a computer or reading a textbook, I love to get my hands dirty with crafting. Everything from pottery and painting to sewing and knitting to making music and podcasts. I love to make things that are sometimes beautiful and sometimes functional. I believe that creating something with your own hands is one of the most satisfying experiences life can offer.

And last but not least, I am a cat person through and through. I firmly believe that cats are the ultimate stress-relievers and mood-boosters.

So, come join me on my journey of exploration, creation, and cat-appreciation. You'll never know what you might learn or create.

## Some of My Other Endeavors

* [Ephemeral Existence Podcast](https://ephemeralexistence.ca/)
* [SewQuest Podcast](https://sewquest.ca/)
* [Send Some Love Project](https://sendsomeloveproject.com/)
