---
layout: page
title: The Mind in the Machine
permalink: /academia/mind-in-the-machine/
nav-menu: false
---

# The Mind in the Machine: How Theory of Mind Capabilities in Generative AI Agents Impact Impressions


## Abstract

As Generative Artificial Intelligence (GenAI) rapidly advances, it is
crucial to understand how its human-like capabilities impact
interactions with users. This study examines impressions of GenAI
demonstrating cognitive and affective facets of theory of mind (ToM) in
chat-based interactions . Participants (N = 159) evaluated transcripts
between human-human, human-GenAI, or GenAI-GenAI pairs under varying ToM
conditions. Preliminary findings reveal that, across all agent types,
affective ToM increases perceptions of eeriness but also enhances
anthropomorphism and likeability. These mixed effects on impressions
highlight the delicate balance between advanced capabilities and natural
interactions in GenAI design .


![Study Poster - Preliminary Results](https://gitlab.com/psychology-projects/uncanny-mind-study/-/raw/main/presentation/love-poster-2024.jpg)

* [Poster PDF](https://gitlab.com/psychology-projects/uncanny-mind-study/-/raw/main/presentation/love-poster-2024.pdf?ref_type=heads&inline=false) 
* [Code Repository](https://gitlab.com/psychology-projects/uncanny-mind-study)
