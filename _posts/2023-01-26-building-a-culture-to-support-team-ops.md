---
layout: post
title:  "Building a Culture to Support TeamOps"
date:   2023-01-26
categories: organizational-behaviour teamops
---

# A Primer on TeamOps
If you are already familiar with [TeamOps](https://about.gitlab.com/teamops/), feel free to skip ahead. In traditional GitLab low-context communication fashion, I’m including a primer with the basics. For more detailed information, be sure to check out the official [TeamOps page](https://about.gitlab.com/teamops/).

TeamOps is a framework that can be applied to any organization, regardless of where you work. You don’t need to be a remote-only organization to get the value that TeamOps offers. The framework consists of a set of Guiding Principles implemented through corresponding Action Tenets.

## The 4 Guiding Principles
### Shared Reality
Optimize for knowledge retrieval over knowledge transfer. Characterized by tenets such as `public by default`, `low-context communication`, and `shared values`

### Everyone Contributes
No matter their level, function, or location, everyone contributes. Characterized by tenets such as `asynchronous workflows`, `give agency`, and `short toes`.

### Decision Velocity
Success is correlated with decision velocity. Characterized by tenets such as `push decisions to the lowest possible level`, `bias for action`, and `collaboration is not consensus`.

### Measurement Clarity
Measure the right things. Characterized by tenets such as `measure results, not hours`, `iteration`, and `prioritize release cadence over scope`.

There are more Action Tenets within each principle, I’ve only highlighted a few here to give you a better sense of what TeamOps is all about.


# Building a Culture to Support TeamOps
So you’re interested in [TeamOps](https://about.gitlab.com/teamops/), or maybe you’ve heard about “The GitLab Way”, and you are interested in implementing it in your own organization. That’s great! It’s also, likely, easier said than done. In the rest of this post I’ll go over an approach to getting started on the path to a successful change. 

## What is Organizational Culture?
Formally, organizational culture is the shared set of beliefs, values, and assumptions that exist within an organization. In this context, “shared” means that members of the organization have uniform exposure to these, and some minimum common understanding of them. It does not mean that they necessarily personally hold those beliefs.  Out of these beliefs, values, and assumptions, norms develop. From these norms, patterns of behaviour emerge. 

Less formally, it’s the day-to-day way of life in the organization - the style, the atmosphere, the personality of the organization. 

Culture is fairly stable over time and can persist even when members come and go. Culture can also have a strong impact on the performance of the organization and member satisfaction. 

## Why are We Talking About Culture?
You may have heard the famous Peter Drucker quote, “Culture Eats Strategy for Breakfast”. In other words, no matter how good your strategy for success is, if your organization’s culture doesn’t support it, you are unlikely to succeed. Culture is also reported as the main reason for failure of organizational change programs.  So if you don’t have a culture that supports the end goal, and you just attempt to make that change, you are likely to fail. 

If you want to successfully follow the Guiding Principles and implement the Action Tenets of TeamOps in your organization, you will need a culture that supports them. If you already have such a culture, you’re in luck! You probably won’t have too much trouble making the necessary changes. If, however, you don’t, your task is going to be harder. Culture is one of the most important and difficult things to change within an organization.  

The first step in cultural change is to diagnose your culture. This allows you to create a clear image of where you are starting from. With that understanding, you can identify what needs to change to support the TeamOps tenets you are looking to incorporate. 

## Analyzing a Culture
Sometimes organizational culture seems more like a feeling than a tangible thing you can fully describe, and it isn’t until you start to compare one culture with another or you are in the midst of a cultural change that the specifics become obvious. The process of analyzing a culture helps you understand it before you are in the midst of change. 

A common pitfall for organizations attempting to make a change is to take the change program of another organization and apply it, as-is, to themselves. Analyzing your organizational culture in the planning stage will allow you to identify the specific needs of your organization. By customizing the plan to the needs, you optimize for success.

There are a few key aspects that you can use to help understand your organizational culture. These are the symbols that are used, the rituals that are followed, and the stories that are told. 

### Symbols 
Symbolism can be a strong indicator of what is valued in the culture. For example, picture the founder of a successful corporation that values being fiscally responsible driving an old economy sedan.

### Rituals
The rites, rituals, and ceremonies that occur within an organization can embody the essence of the culture. For example, at GitLab we now have an established tradition of team members taking off one day per month for Friends & Family day. This embodies our Family First value. 

### Stories
This is the Folklore of the organization. We all know these stories. They are the ones that are told to successive generations as they are learning the ropes. The ones that speak of glory or folly. Researchers have identified 7 themes that are common and run throughout the stories of organizations. Think about the stories of your organization around these themes:
* Rule-breaking
* Is the Big Boss Human?
* Can the Little Person Rise to the Top?
* Will I Get Fired?
* Will the Organization Help Me When I Have to Move?
* How Will the Boss React to Mistakes?
* How Will the Organization Deal with Obstacles?
Within each of these themes there may be stories with positive or negative endings, all illustrating different aspects of the culture. 

### Strong Culture
Beliefs, values, and assumptions are intense and pervasive in a strong culture. This makes a strong culture easier to identify. They are strongly supported by a majority of members of the organization and there is a strong consensus on what the organization stands for. This, however, doesn’t necessitate blind conformity to the existing culture. A strong culture is often associated with greater success and effectiveness. They are also, often, more resistant to change as they are so ingrained throughout the organization.

### Subcultures
Subcultures can develop within organizations. These often reflect the differences that exist in separate departments or within separate occupational areas. These subcultures can range in how similar they are to the overarching culture. 

### Resistance 
Resistance occurs when people fail to support the change effort, whether overtly or covertly. 

Some cultures reward stability and tradition. In these cultures, it is often difficult to implement change as it goes against what is seen as valuable.

There are numerous reasons for resistance, but they mainly fall into two themes. The first being that people feel the change is unnecessary because there is only a small gap between where you are and what’s proposed. In essence, it isn’t worth the effort to change. The second is, people feel that the change is unobtainable because the gap is too large. You are asking them to do the impossible. 


Now the real fun starts…

## Analyze Your Organization
The most fundamental requirement of changing culture is that the people must have the capability, the opportunity, and the motivation to change. 

Here are some prompts to start mapping out your way to implementing TeamOps today!

### Cultural Description
* What is symbolic in your organization? 
* What sorts of rites, rituals, or ceremonies are meaningful in the organization?
* What is the folklore of the organization? What stories are told to successive generations of members to explain how things work?
* Does your organization have subcultures that you need to analyze as well?
* Does your organization have a strong culture?

Now that you’ve identified where you are starting from, go back to the TeamOps tenets, capture the tenets that you want to start with. 

### Identify Where You Will Meet Resistance
For each of those tenets identify what in your current culture poses a risk to implementation.  
* Will organizational members understand the need for the change in the same way that you do? 
	* Does the change seem too small?
		* Is it clear what will be gained by making the change?
	* What can you do to ensure that the vision is clear?
* Does the change seem impossibly large?
	* What can you do to ensure that there are small, manageable steps that can be taken?
* Are there political dynamics or self-interests that could cause friction?
	* Will some feel a loss of power or autonomy in the change?
	* How can you prevent or address those concerns?
* Will organizational members trust the motives behind the proposed change? 
	* What can you do to ensure that the motivation is transparent? 
	* What can you do to build trust ahead of change?
* What strong emotions are likely to occur? Change is hard, and it is likely to cause strong emotions for some.
	* How can these be avoided or soothed?
	* Can you approach the change with empathy?
* Does your current culture support change?
	* Are stability and tradition valued above change?
	* Are there members who strongly identify with the existing organization?
		* What can you do to ensure they do not feel at risk of losing their own identity or their connection to the organization?

## Next Steps
After going through these prompts you likely have an idea of what you would like to accomplish and what challenges you may face along the way. You have a solid foundation for creating an informed implementation plan for TeamOps in your organization.  


# References
Johns, G., & Saks, A. M. (2020). Organizational behaviour: Understanding and managing life at work. Pearson Canada Inc. 
