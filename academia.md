---
layout: page
title: Academia
permalink: /academia/
nav-menu: true
---


# Presentations

## The Mind in the Machine

* [Materials](https://carolinesimpson.ca/academia/mind-in-the-machine/)

## Prune To Bloom

* [UWaterloo PDC 2024 Presentation - Video](https://youtu.be/GRE52LdA5pk) 
* [PDF Slides & Presenter's Notes](https://gitlab.com/cpsimpson/cascade-correlation-network-simulator/-/raw/main/presentation/Prune%20to%20Bloom.pdf?ref_type=heads)
* [Powerpoint Deck](https://gitlab.com/cpsimpson/cascade-correlation-network-simulator/-/raw/main/presentation/Prune%20to%20Bloom.pptx?ref_type=heads)
* [Full Reference List](https://gitlab.com/cpsimpson/cascade-correlation-network-simulator/-/blob/main/presentation/references.md)
* [Code Repository](https://gitlab.com/cpsimpson/cascade-correlation-network-simulator)
